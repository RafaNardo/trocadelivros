import React, { Component } from 'react'
import INSTITUICAO_ENSINO from '../../../constants/instituicoesEnsino'

class EducationaInstitution extends Component {

    constructor(props){
        super(props)

        this.state = { 
            exibirOpcaoTodos: this.props.exibirOpcaoTodos
        }
    }

    renderInstituicaoEnsino() {
        const options = Object.values(INSTITUICAO_ENSINO)

        if (this.state.exibirOpcaoTodos)       
            options.unshift({id: 0, descricao: 'Todos'})
            
        return (
            options.map((instituicao) => <option key={instituicao.id} value={instituicao.id}>{instituicao.descricao}</option>)
        )
    }

    render() {
        return (
            <label className="form-group">
                <span>Instituição de Ensino</span>
                <select name="instituicaoEnsino" className="form-control" value={this.props.instituicaoEnsino}
                    onChange={this.props.handleInputChange} tabIndex="5">
                    {this.renderInstituicaoEnsino()}
                </select>
            </label>
        );
    }
}
export default EducationaInstitution