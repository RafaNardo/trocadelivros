import React, { Component } from 'react'
import FileBase64 from 'react-file-base64'

class UploadImage extends Component {

    getFiles = (imagem) => {
        this.props.callbackImage(imagem)
    }

    render() {
        return (
            <div className="form-group">
                <FileBase64
                    multiple={false}
                    onDone={this.getFiles} />
            </div>
        )
    }
}
export default UploadImage