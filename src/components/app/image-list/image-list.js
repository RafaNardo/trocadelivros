import React, { Component } from 'react'
import './image-list.scss'

class ImageList extends Component {

  defineClassName() {
    return this.props.isPerson ? "perfil__person-avatar" : "item-image"
  }

  render() {
    let imagem = this.props.imagem
    const defaultImageBook = "https://image.flaticon.com/icons/png/128/167/167756.png"
    const defaultImagePerson = "https://image.flaticon.com/icons/svg/74/74472.svg"
    const defaultImage = this.props.isPerson ? defaultImagePerson : defaultImageBook
    const alt = this.props.isPerson ? "foto de perfil" : "capa do livro"
    const className = this.defineClassName()
    return (
      <div className="card-item__smallColumn">
        <img className={className} src={!!imagem ? imagem.base64 : defaultImage } alt={alt} />
      </div>
    )
  }
}
export default ImageList