import React, { Component } from 'react'
import './chat.scss'
import FirebaseService from "../../services/firebaseService";
import UsuariosService from '../../services/usuariosService'
import LivrosService from '../../services/livrosService'
import ImageList from '../image-list/image-list'
import { Link } from 'react-router-dom'

class DadosConversa extends Component {

    constructor(props) {
        super(props)

        this.state = {
            conversa: props.conversa,
            destinatario: {},
            livro: {},
            usuarioLogado: FirebaseService.validarUsuarioLogado(this)
        }
    }

    componentDidMount() {
        if (this.state.conversa) {
            this.obterDadosDestinatario();
            this.obterLivro();
        }
    }

    obterDadosDestinatario() {
        UsuariosService.obterUsuarioPeloUid(this.obterIdUsuarioDestinatario(), destinatario => {
            
            this.setState({
                destinatario
            })
        });
    }
    
    obterLivro() {
        LivrosService.obterLivro(this.state.conversa.livro, livro => {
            this.setState({
                livro
            })
        });
    }

    obterIdUsuarioDestinatario(){
        const conversa = this.state.conversa

        const { donoDoLivro, interessado } = conversa;

        return this.state.usuarioLogado.uid === donoDoLivro ? interessado : donoDoLivro
    }

    render() {
        let index = 0

        const conversa = this.state.conversa

        if (!this.state.livro.disponivelParaTroca){
            return (
                <div hidden={true}></div>
            );
        }
        else {
            return (
                <div className="card-item-wrapper" key={index++}>
                    <div className="card-item">
                        <ImageList imagem={this.state.destinatario.fotoDestinatario} isPerson={true} />
    
                        <div className="card-item__column-left">
                            <strong className="item-brand">{this.state.destinatario.nome}</strong>
                            <span className="item-info">
                                Conversa sobre o livro <strong>{this.state.livro.titulo}</strong>
                            </span>
                        </div>
                        
                        <div className="card-item__column">
                            <Link to={`/Chat/${conversa.livro}/${conversa.donoDoLivro}/${conversa.interessado}`}>Visualizar</Link>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

export default DadosConversa
