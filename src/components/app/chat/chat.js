import React, { Component } from 'react'
import FirebaseService from "../../services/firebaseService";
import Messagens from "./mensagens";
import Cabecalho from "./cabecalho";
import './chat.scss'

class Chat extends Component {

    constructor(props) {
        super(props)

        const { livro, donoDoLivro, interessado } = this.props.match.params;
        
        this.state = { 
            livro, 
            donoDoLivro, 
            interessado, 
            mensagens: [],
            usuarioLogado: FirebaseService.validarUsuarioLogado(this).uid
        };

        this.obterUsuarioDestinatario = this.obterUsuarioDestinatario.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.enviarMensagem = this.enviarMensagem.bind(this)
        this.obterConversa = this.obterConversa.bind(this)
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name

        this.setState({ [name]: value })
    }

    componentDidMount() {
        this.obterConversa()
    }

    obterUsuarioDestinatario(){
        const { donoDoLivro, interessado, usuarioLogado } = this.state;

        return donoDoLivro === usuarioLogado ? interessado : donoDoLivro
    }

    obterConversa(){
        FirebaseService.getDataList('conversas', (result) => {
            const conversa = result.find(data => {
                                return data.livro === this.state.livro &&
                                        data.donoDoLivro === this.state.donoDoLivro && 
                                        data.interessado === this.state.interessado
                            })

            if (conversa) {
                this.setState({ 
                    id: conversa.key,
                    livro: conversa.livro,
                    donoDoLivro: conversa.donoDoLivro,
                    interessado: conversa.interessado,
                    mensagens: conversa.mensagens || [],
                })
            }
        })
    }

    enviarMensagem(){

        const conversa = { 
            livro: this.state.livro,
            donoDoLivro: this.state.donoDoLivro,
            interessado: this.state.interessado,
            mensagens: this.state.mensagens
        }

        if(this.state.mensagem) {
            var currDate = new Date()

            conversa.mensagens.push({
                texto: this.state.mensagem,
                horario: `${currDate.toLocaleDateString()} ${currDate.getHours()}:${currDate.getMinutes()}`,
                remetente: this.state.usuarioLogado
            })
            
            if (this.state.id) {
                FirebaseService.updateData(this.state.id, 'conversas', conversa)
            }
            else {
                FirebaseService.pushData('conversas', conversa);
            }

            this.setState({mensagem: ''})
        }
    }

    render() {
        return (
        <div className="content">
            <div className="chat">

                <Cabecalho destinatario={this.obterUsuarioDestinatario()}/>
            
                <Messagens mensagens={this.state.mensagens} usuarioLogado={this.state.usuarioLogado} />
        
                <div className="chat-message clearfix">
                    <textarea name="mensagem" placeholder ="Type your message" rows="3"
                                value={this.state.mensagem} onChange={this.handleInputChange}></textarea>
                    <button onClick={this.enviarMensagem}>Send</button>
                </div>
            </div>
        </div>
        )
    }
}

export default Chat
