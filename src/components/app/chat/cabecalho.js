import React, { Component } from 'react'
import './chat.scss'
import UsuariosService from '../../services/usuariosService'
import ImageList from '../../app/image-list/image-list'

class Cabecalho extends Component {

    constructor(props) {
        super(props)

        this.state = { 
            destinatarioId: props.destinatario 
        }
    }

    componentDidMount() {
        this.obterDadosDestinatario();
    }

    obterDadosDestinatario() {
        UsuariosService.obterUsuarioPeloUid(this.state.destinatarioId, destinatario => {
            this.setState({
                nome: destinatario.nome,
                foto: destinatario.imagem,
            })
        });
    }

    render() {
        return (
            <div className="chat__head">
                <div className="contact-details">
                    <ImageList imagem={this.state.foto} isPerson={true} className="contact-details__picture" />
                    <span className="contact-details__name">
                        {this.state.nome}
                    </span>
                </div>
            </div>
        )
    }
}

export default Cabecalho
