import React, { Component } from 'react'
import FirebaseService from "../../services/firebaseService";
import DadosConversa from "./dadosConversa"

class MinhasConversas extends Component {

    constructor(props) {
        super(props)

        this.state = {
            conversas: [],
            user: FirebaseService.validarUsuarioLogado(this),
        };
    }

    componentDidMount() { 
        if (this.state.user) {
            this.obterConversas();
        }
    }

    filtarConversas(event){
        this.obterConversas(event.target.value);
    }

    obterConversas() {
        FirebaseService.getDataList("conversas", conversas => {
            const conversasUsuario = conversas.filter(conversa => {
                    return conversa.donoDoLivro === this.state.user.uid || 
                            conversa.interessado === this.state.user.uid
                })

            this.setState({ 
                conversas: conversasUsuario
            })
        })
    }

    render() {
        return (
        <div className="content">
            <div className="tile-search">
                <label className="form-group">
                    <span>Minhas Conversas</span>
                </label>
            </div>
            {
                this.state.conversas.map(conversa => {
                    return (
                        <div key={conversa.donoDoLivro + conversa.livro}>
                            <DadosConversa conversa={conversa}/>
                        </div>
                    )
                })
            }

        </div>
        )
    }
}

export default MinhasConversas
