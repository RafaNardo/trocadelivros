import React, { Component } from 'react'
import './chat.scss'

class Messagens extends Component {

    constructor(props) {
        super(props)

        const { mensagens, usuarioLogado} = this.props

        this.state = { mensagens, usuarioLogado}
    }

    componentWillReceiveProps(nextProps) {

        const { mensagens, usuarioLogado} = nextProps

        this.setState({ mensagens, usuarioLogado })
      }

    render() {
        let index = 0

        return (
            <div className="messages">
            {
                this.state.mensagens.map(mensagem => {
                    
                    const cssClass = mensagem.remetente === this.state.usuarioLogado ? 
                                        "messages__box me" : "messages__box them"

                    return (
                        <div className={cssClass} key={index++}>
                            <div className="messages__plain-text">
                                {mensagem.texto}
                            </div>
                            <div className="messages__timestamp">
                                {mensagem.horario}
                            </div>
                        </div>
                    )
                })
            }
            </div>
        )
    }
}

export default Messagens
