import FirebaseService from "./firebaseService";


export default class ChatService {
    
    obterConversa(livro, donoDoLivro, interessado){
        FirebaseService.getDataList('conversas', (conversas) => {
            return conversas.filter(conversa => {
                return conversa.livro === livro &&
                        conversa.donoDoLivro === donoDoLivro && 
                        conversa.interessado === interessado
            })
        })
    }

}