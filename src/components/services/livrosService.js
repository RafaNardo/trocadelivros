import FirebaseService from "./firebaseService";

export default class LivrosService {
    
    static obterLivro(id, callback) {
        FirebaseService.getUniqueDataBy(id, 'livros', (livro) => {
            return callback(livro);
        });
    }
    
    static obterLivros(usuarioId, titulo) {
        FirebaseService.getDataList("livros/", livros => {
            this.setState({ 
                livros : livros.filter(livro => { 
                    if(!titulo) {
                        return livro.usuario === usuarioId;
                    } else {
                        return livro.usuario === usuarioId && livro.titulo.toLowerCase().includes(titulo.toLowerCase());
                    }
                })
            })
        })
    }

    static filtrarLivros(titulo, instituicaoEnsino, idUsuarioLogado, callback) {
        FirebaseService.getDataList("livros/", livros => {

            const livrosEncontrados = livros.filter(livro => {
                    const livroTemOMesmoNome = !titulo || livro.titulo.toLowerCase().includes(titulo.toLowerCase());
                    const livroNaMesmaInstituicao = !Number(instituicaoEnsino) || parseInt(livro.instituicaoEnsino, 10) === parseInt(instituicaoEnsino, 10);
                    const livroEstaDisponivel = !livro.trocado
                    const naoPertenceAoUsuarioLogado = livro.usuario !== idUsuarioLogado
                    const estaDisponivelParaTroca = livro.disponivelParaTroca
                    return livroTemOMesmoNome && 
                            livroNaMesmaInstituicao && 
                            livroEstaDisponivel && 
                            naoPertenceAoUsuarioLogado &&
                            estaDisponivelParaTroca
                })

            callback(livrosEncontrados)
        })
    }

    static excluirLivro(key, callback){
        FirebaseService.remove(key, "livros");

        if (callback) {
            callback();
        }
    }
}

