import {firebaseDatabase, firebaseAuth} from '../utils/firebaseUtils'

export default class FirebaseService {
    static getDataList = (nodePath, callback, size) => {

        let query = firebaseDatabase.ref(nodePath);

        if (size)
            query.limitToLast(size);

        query.on('value', dataSnapshot => {
            let items = [];
            dataSnapshot.forEach(childSnapshot => {
                let item = childSnapshot.val();
                item['key'] = childSnapshot.key;
                items.push(item);
            });
            callback(items);
        });

        return query;
    };

    static getUniqueDataBy = (id, node, callback) => {    
        let query = firebaseDatabase.ref(node + '/' + id);

        query.on('value', dataSnapshot => {
            let item = dataSnapshot.val();
            item['key'] = dataSnapshot.key;
            callback(item);
        });

        return query;
    }

    static pushData = (node, objToSubmit, callback) => {
        const ref = firebaseDatabase.ref(node).push();
        const id = firebaseDatabase.ref(node).push().key;
        ref.set(objToSubmit);

        if (callback) {
            callback(id);
        }

        return id;
    };

    static remove = (id, node) => {
        return firebaseDatabase.ref(node + '/' + id).remove();
    };

    static updateData = (id, node, objToSubmit, callback) => {
        firebaseDatabase.ref(node + '/' + id).update(objToSubmit);
        
        if (callback) {
            callback(id);
        }
    }

    static registerUser = (email, password, callback) => {
        firebaseAuth.createUserWithEmailAndPassword(email, password)
        .then((response) => {
            if(callback) {
                callback({ error: null, user: response.user });
            }
        })
        .catch(function(error) {
            if (callback) {
                callback({ error: error, user: null });
            }
        })
    }
    
    static signIn(email, password, callback) {
        firebaseAuth.signInWithEmailAndPassword(email, password)
        .then((response) => {

            var user = response.user;

            if(callback) {
                sessionStorage.setItem('user', JSON.stringify(user));
                callback({ error: null, user: user });
            }
        })
        .catch((error) => {
            if (callback) {
                callback({ error: error, user: null });
            }
        });
    }

    static logout() {
        firebaseAuth.signOut()
        .then(() => {
            sessionStorage.removeItem('user')
        })
    }

    static validarUsuarioLogado(self, callback){
        var user = JSON.parse(sessionStorage.getItem('user'));
        
        if (!user) {
            alert("Você precisa estar logado para acessar este recurso");
            
            self.props.history.push('/Login');
        }

        if (callback) {
            callback(user);
        }

        return user;
    }
}