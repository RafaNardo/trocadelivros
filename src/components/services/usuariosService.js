import FirebaseService from "./firebaseService";

export default class UsuariosService {
    
    static obterUsuarioPeloUid(uid, callback) {
        FirebaseService.getDataList('usuarios', (usuarios) => {
            return callback(usuarios.find(usuario => {
                    return usuario.uid === uid;
                })
            );
        });
    }
}

