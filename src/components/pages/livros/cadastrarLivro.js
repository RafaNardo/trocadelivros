import React, { Component } from 'react'
import FirebaseService from "../../services/firebaseService"
import { Redirect } from 'react-router'
import FileBase64 from 'react-file-base64'
import EducationaInstitution from '../../app/educational-institution/educational-institution'

class CadastrarLivro extends Component {

    constructor(props) {
        super(props)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.salvarLivro = this.salvarLivro.bind(this)
        this.voltar = this.voltar.bind(this)

        this.state = {
            id: this.props.match.params.id,
            titulo: "",
            autor: "",
            editora: "",
            tipoDeCapa: 1,
            paginas: 100,
            instituicaoEnsino: 1,
            imagem: '',
            disponivelParaTroca: true,
            user: FirebaseService.validarUsuarioLogado(this)
        };
    }

    componentDidMount() {
        if (this.state.id) {
            FirebaseService.getUniqueDataBy(this.state.id, 'livros', (livro) => {
                this.setState({
                    id: this.state.id,
                    titulo: livro.titulo,
                    autor: livro.autor,
                    editora: livro.editora,
                    tipoDeCapa: livro.tipoDeCapa,
                    paginas: livro.paginas,
                    instituicaoEnsino: livro.instituicaoEnsino,
                    imagem: livro.imagem,
                    disponivelParaTroca: livro.disponivelParaTroca,
                });
            });
        }
    };

    voltar() {
        this.props.history.push('/MeusLivros');
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name

        this.setState({
            [name]: value
        })
    }

    renderTiposDeCapa() {
        let options = [
            { id: 1, descricao: "Capa Comum" },
            { id: 2, descricao: "Capa Grossa" },
            { id: 3, descricao: "Outra Capa" }
        ];

        return (options.map((tipoDeCapa) => <option key={tipoDeCapa.id} value={tipoDeCapa.id}>{tipoDeCapa.descricao}</option>))
    }

    salvarLivro() {
        const livro = {
            titulo: this.state.titulo,
            autor: this.state.autor,
            editora: this.state.editora,
            tipoDeCapa: this.state.tipoDeCapa,
            paginas: this.state.paginas,
            usuario: this.state.user.uid,
            instituicaoEnsino: this.state.instituicaoEnsino,
            imagem: this.state.imagem,
            disponivelParaTroca: this.state.disponivelParaTroca
        }
        
        if (this.state.id) {
            FirebaseService.updateData(this.state.id, 'livros', livro)
        }
        else {
            FirebaseService.pushData('livros/', livro);
        }

        this.setState({ redirect: true });
    }

    getFiles = (imagem) => {
        this.setState({ imagem })
    }

    render() {

        if (this.state.redirect) {
            return (<Redirect to='/MeusLivros' />);
        }

        return (
            <div className="content">
                <form className="cadastroDeLivro" onSubmit={this.salvarLivro}>

                    <label className="form-group">
                        <span>Titulo</span>
                        <input type="text" className="form-control" name="titulo"
                            value={this.state.titulo} onChange={this.handleInputChange} autoFocus={true} tabIndex="1" />
                    </label>

                    <div className="form-group">
                        <span>Autor</span>
                        <input type="text" name="autor" className="form-control"
                            value={this.state.autor} onChange={this.handleInputChange} tabIndex="2" />
                    </div>

                    <div className="form-group">
                        <span>Editora</span>
                        <input type="text" name="editora" className="form-control"
                            value={this.state.editora} onChange={this.handleInputChange} tabIndex="3" />
                    </div>

                    <div className="form-group">
                        <span>Tipo de Capa</span>
                        <select name="tipoDeCapa" className="form-control" value={this.state.tipoDeCapa}
                            onChange={this.handleInputChange} tabIndex="4">
                            {this.renderTiposDeCapa()}
                        </select>
                    </div>

                    <div className="form-group">
                        <span>Páginas</span>
                        <input type="number" name="paginas" className="form-control"
                            value={this.state.paginas} onChange={this.handleInputChange} tabIndex="5" />
                    </div>

                    <EducationaInstitution handleInputChange={this.handleInputChange} instituicaoEnsino={this.state.instituicaoEnsino} />

                    <div className="form-group">
                        <span>Imagem</span>
                        <br/>
                        <FileBase64 multiple={false} onDone={this.getFiles} className="form-control" />
                    </div>

                    <div className="form-group">
                        <span>Disponível para Troca</span>
                        <br/>
                        <input type="checkbox" name="disponivelParaTroca"
                            checked={this.state.disponivelParaTroca} onChange={this.handleInputChange} tabIndex="6" />
                    </div>

                    <button className="button" type="submit" tabIndex="7">Salvar</button>
                </form>

                <div>
                    <button className="button" onClick={this.voltar}>Voltar</button>
                </div>
            </div>
        )
    }
}

export default CadastrarLivro
