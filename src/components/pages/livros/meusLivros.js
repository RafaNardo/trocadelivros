import React, { Component } from 'react'
import FirebaseService from "../../services/firebaseService";
import { Link } from 'react-router-dom'
import ImageList from '../../app/image-list/image-list'

class MeusLivros extends Component {

    constructor(props) {
        super(props)

        this.state = {
            livros: [],
            user: FirebaseService.validarUsuarioLogado(this),
        };

        this.excluirLivro = this.excluirLivro.bind(this)
        this.cadastrarLivro = this.cadastrarLivro.bind(this)
        this.filtrarLivros = this.filtrarLivros.bind(this)
    }

    componentDidMount() { 
        if (this.state.user) {
            this.obterLivros(this.state.user.uid);
        }
    }

    filtrarLivros(event){
        this.obterLivros(this.state.user.uid, event.target.value);
    }
    
    obterLivros(usuarioId, titulo) {
        FirebaseService.getDataList("livros/", livros => {
            this.setState({ 
                livros : livros.filter(livro => { 
                    if(!titulo) {
                        return livro.usuario === usuarioId;
                    } else {
                        return livro.usuario === usuarioId && livro.titulo.toLowerCase().includes(titulo.toLowerCase());
                    }
                })
            })
        })
    }

    cadastrarLivro(){
        this.props.history.push('CadastrarLivro');
    }

    excluirLivro(key){
        FirebaseService.remove(key, "livros");
    }

    render() {
        return (
        <div className="content">
            <div className="tile-search">
                <label className="form-group">
                    <span>Meus Livros</span>
                    <input type="text" className="form-control" name="Pesquisar" placeholder="Pesquisar em meus livros"
                            onChange={this.filtrarLivros} autoFocus={true} tabIndex="1"/>
                </label>
            </div>

            {
	            this.state.livros.map(livro =>{
                return (
                <div className="card-item-wrapper" key={livro.key}>
                    <div className="card-item">
                        <ImageList imagem={livro.imagem} />
                        <div className="card-item__column">
                            <strong className="item-brand">{livro.titulo}</strong>
                            <span className="item-info">
                                {livro.autor} - {livro.editora}
                            </span>
                        </div>
                        <div className="card-item__column">
                            <Link to={`/CadastrarLivro/${livro.key}`}>Editar</Link>
                            <Link to="#" onClick={() => this.excluirLivro(livro.key)}>Excluir</Link>
                        </div>
                    </div>
                </div>
                )})
            }
            
            <div className="tile-search">
                <button className="button" onClick={this.cadastrarLivro}>Cadastrar Livro</button>
            </div>
        </div>
        )
    }
}

export default MeusLivros
