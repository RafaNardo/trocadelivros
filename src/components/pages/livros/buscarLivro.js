import React, { Component } from 'react'
import FirebaseService from "../../services/firebaseService";
import LivrosService from '../../services/livrosService'
import { Link } from 'react-router-dom'
import ImageList from '../../app/image-list/image-list'
import EducationaInstitution from '../../app/educational-institution/educational-institution'

class BuscarLivro extends Component {

    constructor(props) {
        super(props)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.pesquisarLivros = this.pesquisarLivros.bind(this)
        this.atualizarState = this.atualizarState.bind(this)

        this.state = {
            titulo: '',
            instituicaoEnsino: 0,
            livrosEncontrados: [],
            user: FirebaseService.validarUsuarioLogado(this)
        };
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name

        this.setState({
            [name]: value
        })

        setTimeout(() => {
            this.pesquisarLivros()
        }, 10);

    }

    componentDidMount(){
        this.pesquisarLivros()
    }

    pesquisarLivros() {
        const user = this.state.user

        if(user) {
            LivrosService.filtrarLivros(this.state.titulo, 
                                this.state.instituicaoEnsino, 
                                user.uid,
                                this.atualizarState)
        }
    }

    atualizarState(livrosEncontrados) {
        this.setState({ livrosEncontrados })
    }

    render() {
        return (
            <div className="content">
                <div className="tile-search">
                    <label className="form-group">
                        <span>Buscar Livros</span>
                    </label>
                </div>

                <div className="filter-search">
                    <label className="form-group">
                        <span>Titulo</span>
                        <input type="text" className="form-control" name="titulo"
                            value={this.state.titulo} onChange={this.handleInputChange} autoFocus={true} tabIndex="1" />
                    </label>

                    <EducationaInstitution 
                        exibirOpcaoTodos={true}
                        handleInputChange={this.handleInputChange} 
                        instituicaoEnsino={this.state.instituicaoEnsino} />
                </div>
                <div className="content-search">
                    {this.state.livrosEncontrados.map(livro => {
                        return (
                            <div className="card-item-wrapper" key={livro.key}>
                                <div className="card-item">
                                    <ImageList imagem={livro.imagem} />
                                    <div className="card-item__column">
                                        <strong className="item-brand">{livro.titulo}</strong>
                                        <span className="item-info">
                                            {livro.autor} - {livro.editora}
                                        </span>
                                    </div>
                                    <div className="card-item__column">
                                        <Link to={`/Chat/${livro.key}/${livro.usuario}/${this.state.user.uid}`}>Sugerir troca</Link>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export default BuscarLivro
