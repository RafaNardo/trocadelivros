import React, { Component } from 'react'
import FirebaseService from "../../services/firebaseService";

class Login extends Component {

    constructor(props) {
        super(props)
        this.handleInputChange = this.handleInputChange.bind(this);
        this.logar = this.logar.bind(this);
        this.registrar = this.registrar.bind(this);

        this.state = {
            email: "",
            senha: ""
        };
    }
    
    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name

        this.setState({
            [name]: value
        })
    }

    registrar(){
        this.props.history.push('/Registro');
    }

    logar() {        
        const usuario = { 
            email: this.state.email,
            senha: this.state.senha
        }

        FirebaseService.signIn(usuario.email, usuario.senha, (response) => {
            if (response.error) {
                alert(`${response.error.code} - ${response.error.message}`);
            } else {
                this.props.history.push('/BuscarLivros');
            }
        });
    }
    
    render() {
        return (
        <div className="content">
            <h1>Login</h1>
            <div className="cadastroDeUsuario">
                <div className="form-group">
                    <span>Email</span>
                    <input type="text" name="email" className="form-control" 
                            value={this.state.email} onChange={this.handleInputChange} tabIndex="1"/>
                </div>
                <div className="form-group">
                    <span>Senha</span>
                    <input type="password" name="senha" className="form-control" 
                            value={this.state.senha} onChange={this.handleInputChange} tabIndex="2"/>
                </div>
                <button className="button" tabIndex="3" onClick={this.logar}>Login</button>
                <button className="button" tabIndex="3" onClick={this.registrar}>Registro</button>
            </div>
        </div>
        )
    }
}

export default Login
