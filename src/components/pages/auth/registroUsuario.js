import React, { Component } from 'react'
import FirebaseService from "../../services/firebaseService";
import EducationaInstitution from '../../app/educational-institution/educational-institution'
import UploadImage from '../../app/upload-image/upload-image'

class RegistroUsuario extends Component {

    constructor(props) {
        super(props)
        this.handleInputChange = this.handleInputChange.bind(this);
        this.salvarUsuario = this.salvarUsuario.bind(this);
        this.voltar = this.voltar.bind(this);

        this.state = {
            nome: "",
            email: "",
            senha: "",
            confirmarSenha: "",
            instituicaoEnsino: 1,
            imagem: ""
        };
    }

    voltar(){
        this.props.history.push('/');
    }
    
    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name

        this.setState({
            [name]: value
        })
    }

    salvarUsuario() {
        if (this.state.senha !== this.state.confirmarSenha) {
            alert("Os campos 'Senha'e 'Confirmação de Senha' precisam ser iguais.");

            return false;
        }
        
        FirebaseService.registerUser(this.state.email, this.state.senha, (response) => {
            if (response.error) {
                alert(`${response.error.code} - ${response.error.message}`);
            } else {                
                FirebaseService.pushData('usuarios/', {
                    nome: this.state.nome,
                    email: this.state.email,
                    instituicaoEnsino: this.state.instituicaoEnsino,
                    imagem: this.state.imagem,
                    uid: response.user.uid
                });
                this.props.history.push('/');
                alert('Você foi registrado com sucesso');
            }
        });
    }

    callbackImage = (imagem) => {
        this.setState({ imagem })
    }
    
    render() {
        return (
        <div className="content">
            <div className="cadastroDeUsuario">

            <label className="form-group">
                <span>Nome</span>
                <input type="text" className="form-control" name="nome" 
                        value={this.state.nome} onChange={this.handleInputChange} autoFocus={true} tabIndex="1"/>
            </label>

            <div className="form-group">
                <span>Email</span>
                <input type="text" name="email" className="form-control" 
                        value={this.state.email} onChange={this.handleInputChange} tabIndex="2"/>
            </div>

            <div className="form-group">
                <span>Senha</span>
                <input type="password" name="senha" className="form-control" 
                        value={this.state.senha} onChange={this.handleInputChange} tabIndex="3"/>
            </div>

            <div className="form-group">
                <span>Confirmar Senha</span>
                <input type="password" name="confirmarSenha" className="form-control" 
                        value={this.state.confirmarSenha} onChange={this.handleInputChange} tabIndex="4"/>
            </div>
            
            <EducationaInstitution handleInputChange={this.handleInputChange} instituicaoEnsino={this.state.instituicaoEnsino} />

            <UploadImage callbackImage={this.callbackImage} />

            <button className="button" onClick={this.salvarUsuario} tabIndex="6">Salvar</button>
            </div>
            
            <div>
                <button className="button" onClick={this.voltar}>Cancelar</button>
            </div>
        </div>
        )
    }
}

export default RegistroUsuario
