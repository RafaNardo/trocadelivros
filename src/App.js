import React, { Component } from 'react'
import { Router, Route, Link, Switch } from 'react-router-dom'
//import Cadastro from './components/pages/cadastro/cadastro'
import logo from './img/logoTemporario.png'
import IconHome from './components/icons/icon-home'
import IconList from './components/icons/icon-list'
import IconLogout from './components/icons/icon-logout'
import IconMessages from './components/icons/icon-messages'
import createBrowserHistory from 'history/createBrowserHistory'
import BuscarLivro from './components/pages/livros/buscarLivro'
import CadastrarLivro from './components/pages/livros/cadastrarLivro'
import MeusLivros from './components/pages/livros/meusLivros'
import RegistroUsuario from './components/pages/auth/registroUsuario'
import Login from './components/pages/auth/login';
import Chat from './components/app/chat/chat';
import MinhasConversas from './components/app/chat/minhasConversas';
import FirebaseService from './components/services/firebaseService'

const customHistory = createBrowserHistory()

class App extends Component {

  handleLogout = () => {
    FirebaseService.logout()
    customHistory.push("/Login");
  }

  render() {
    return (
      <div>
        <Router history={customHistory}>
          <div>
            <div className="header">
              <div className="content">
                <div className="header-column-icon">
                  <Link to="/BuscarLivros">
                    <IconHome />
                  </Link>
                </div>
                <figure className="logo">
                  <img src={logo} alt="Trade Your Book logo" />
                </figure>
                <div className="header-column-icon">
                  <Link to="/MinhasConversas">
                    <IconMessages />
                  </Link>
                  <Link to="/MeusLivros">
                    <IconList />
                  </Link>
                  <Link to="#" onClick={this.handleLogout}>
                    <IconLogout/>
                  </Link>
                </div>
              </div>
            </div>
            <Switch>
              <Route exact path="/" component={Login} />
              <Route path="/Login" component={Login} />
              <Route path="/Registro" component={RegistroUsuario} />
              <Route path="/MeusLivros" component={MeusLivros} />
              <Route exact path="/CadastrarLivro" component={CadastrarLivro} />
              <Route path="/CadastrarLivro/:id" component={CadastrarLivro} />
              <Route path="/BuscarLivros" component={BuscarLivro}/>
              <Route path="/Chat/:livro/:donoDoLivro/:interessado" component={Chat} />
              <Route path="/MinhasConversas" component={MinhasConversas} />
            </Switch>
          </div>
        </Router>
      </div>
    )
  }
}

export default App
